<?php
require_once "./dbConnection.php";
use PDO;

$sql = "SELECT hive_data.date, hive_data.weight, hive_data.temperature, hive_data.humidity, hive.name 
        FROM hive_data 
        LEFT JOIN hive ON hive_data.hive_id = hive.id";
$query = $pdo->prepare($sql);
$query->execute();
$data = $query->fetchAll(PDO::FETCH_ASSOC);

?>

<?php include('./head.php') ?>
    <header>
        <?php include('./header.php') ?>
    </header>
    <div class="container">
        <h2>Informations des ruches</h2>
        <table class="table display" width="100%" id="informations-table">
            <thead>
            <tr class="table-head">
                <th align="left">Ruche</th>
                <th align="left">Date</th>
                <th align="left">Poids</th>
                <th align="left">Température</th>
                <th align="left">Humidité</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        let data = <?php echo json_encode($data); ?>;
        $(document).ready( function () {
            $('#informations-table').DataTable({
                language: {
                    url:"../public/dataTable/language/French.json"
                },
                data: data,
                columns: [
                    { data: 'name' },
                    {data: 'date'},
                    { data: 'weight' },
                    { data: 'temperature' },
                    { data: 'humidity' }
                ]
            });
        } );
    </script>

<?php include('./footer.php') ?>