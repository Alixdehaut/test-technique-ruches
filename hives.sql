CREATE DATABASE IF NOT EXISTS hives DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `hive` (
  `id` int(11) NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`),
  `name` varchar(255) NOT NULL,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `hive` (`id`, `name`, `latitude`, `longitude`) VALUES
(1, 'Ruche 1', 48.862724, -2.712408),
(2, 'Ruche 2', 48.968597, 5.304447);


CREATE TABLE `hive_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`),
  `date` datetime NOT NULL,
  `weight` float UNSIGNED NOT NULL,
  `temperature` float NOT NULL,
  `humidity` float UNSIGNED NOT NULL,
  `hive_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `hive_data` (`id`, `date`, `weight`, `temperature`, `humidity`, `hive_id`) VALUES
(1, '2020-07-18 10:30:00', 3, 22, 30, 1),
(2, '2020-07-18 06:30:00', 5, 18, 34, 1),
(3, '2020-07-18 10:00:00', 3, 22, 30, 2),
(4, '2020-07-18 06:00:00', 5, 18, 34, 2);
