<?php include('./head.php') ?>

<header>
    <?php include('./header.php') ?>
</header>
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">Ajouter une ruche</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" id="addHiveForm" method="POST" action="./Crud/hive/add.php">
                                <div class="form-group">
                                    <label for="name">Nom</label>
                                    <input type="text" class="form-control form-control-lg rounded-0" name="name" id="name" required="">
                                    <div class="invalid-feedback">Oops, you missed this one.</div>
                                </div>
                                <div class="form-group">
                                    <label for="lat">Latitude</label>
                                    <input type="number" step="any" class="form-control form-control-lg rounded-0" name="lat" id="lat" required="">
                                    <div class="invalid-feedback">Oops, you missed this one.</div>
                                </div>
                                <div class="form-group">
                                    <label for="long">Longitude</label>
                                    <input type="number" step="any" class="form-control form-control-lg rounded-0" name="long" id="long" required="">
                                    <div class="invalid-feedback">Oops, you missed this one.</div>
                                </div>
                                <button type="submit" class="btn btn-success btn-lg float-right" id="addHiveBtn">Ajouter</button>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->
<?php include('./footer.php') ?>