<?php include('./head.php') ?>

<header>
    <?php include('./header.php');?>
</header>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <form>
                    <div class="row">
                        <div class="col-3">
                            <div class="rounded-circle border border-dark h-100"></div>
                        </div>
                        <div class="col-9">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Template Name</label>
                                <input type="test" class="form-control" id="exampleFormControlInput1" placeholder="Uncomplete Profile">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Subject</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Hello">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                    </div>
                    <div class="form-group w-50">
                        <label for="exampleFormControlSelect1">Example select</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Email + push</option>
                            <option>Push</option>
                            <option>Email</option>
                        </select>
                    </div>

                    <p>Send to group</p>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            Lorem
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                        <label class="form-check-label" for="defaultCheck2">
                            Lorem
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            Lorem
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" >
                        <label class="form-check-label" for="defaultCheck2">
                            Lorem
                        </label>
                    </div>
                    <button class="btn btn-primary m-4 w-25">Valider</button>
                    <button class="btn btn-outline-dark">Annuler</button>
                </form>
            </div>
            <div class="col-md-4 col-12">
                <div class="card p-1 my-4" style="width: 18rem;">
                    <img src="https://dummyimage.com/cga" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Thumnail label</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Animi consequuntur dolore doloribus illo modi quae reiciendis.
                        </p>
                        <a href="#" class="btn btn-primary">Button</a>
                        <a href="#" class="btn btn-outline-dark">Button</a>
                    </div>
                </div>
                <form class="">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1 text-black-50">Tap Target</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Profile screen</option>
                            <option>Lorem</option>
                            <option>Lorem</option>
                        </select>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label bg-success text-white p-1 rounded small" for="exampleRadios1">
                            News
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label bg-primary text-white p-1 rounded small" for="exampleRadios2">
                           Reports
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
                        <label class="form-check-label bg-warning text-white p-1 rounded small" for="exampleRadios3">
                            Documents
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label bg-primary text-white p-1 rounded small" for="exampleRadios2">
                            Media
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
                        <label class="form-check-label bg-secondary text-white p-1 rounded small" for="exampleRadios3">
                            Text
                        </label>
                    </div>
                </form>
                <button class="btn btn-danger m-4 w-50">Supprimer</button>
            </div>
        </div>
    </div>

<?php include('./footer.php') ?>