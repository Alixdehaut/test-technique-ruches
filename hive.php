<?php
require_once "./dbConnection.php";
use PDO;

    $sql = "SELECT * FROM hive";
    $query = $pdo->prepare($sql);
    $query->execute();
    $data = $query->fetchAll(PDO::FETCH_ASSOC)

?>

<?php include('./head.php') ?>

<header>
    <?php include('./header.php') ?>
</header>
<div class="container">
    <button class="btn btn-success mb-4"><a class="text-white" href="./add.php">Ajouter une ruche</a></button>
    <table class="table display" width="100%" id="hives-table">
        <thead>
        <tr class="table-head">
            <th align="left">Nom</th>
            <th align="left">Latitude</th>
            <th align="left">Longitude</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    let data = <?php echo json_encode($data); ?>;
    $(document).ready( function () {
        var table = $('#hives-table').DataTable({
            language: {
                url: "./public/dataTable/language/French.json"
            },
            data: data,
            columns: [
                { data: 'name' },
                { data: 'latitude' },
                { data: 'longitude' },
                {  "orderable": false,
                    "data": null,
                    "render": (data, type, row, meta) => {
                        return `<div class="text-right">
                                    <a href="./edit.php?id=${data.id}">Modifier</a> /
                                    <a href="./Crud/hive/delete.php?id=${data.id}">Supprimer</a>
                                </div>
                        `
                    }
                },
                ]
        })
    } );
</script>

<?php include('./footer.php') ?>